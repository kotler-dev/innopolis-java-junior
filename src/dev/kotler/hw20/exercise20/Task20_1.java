package dev.kotler.hw20.exercise20;

/*
o001aa111|Camry|Black|133|820
o002aa111|Camry|Green|0|130
o001aa111|Camry|Black|133|820
o003aa111|Camry|Red|133|700
o004aa111|Camry|Yellow|133|780

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map

*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Task20_1 {
    public static void main(String[] args) {
        List<String[]> data = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("src/dev/kotler/hw20/input.txt"))) {
            String readLine = reader.readLine();

            while (readLine != null) {
                data.add(readLine.split("\\|"));
                readLine = reader.readLine();
            }

            List<String> result = data.stream()
                    .filter(x -> x[2].equals("Black") || x[3].equals("0"))
                    .map(x -> x[0])
                    .toList();

            for (String str : result) {
                System.out.println(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
