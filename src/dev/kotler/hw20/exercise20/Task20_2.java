package dev.kotler.hw20.exercise20;

/*
o001aa111|Camry|Black|133|790
o002aa111|Camry|Green|0|130
o001aa111|Camry|Black|133|790
o003aa111|Camry|Red|133|700
o004aa111|Camry|Yellow|133|810

Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Task20_2 {
    public static void main(String[] args) {
        Map<String, String[]> data = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("src/dev/kotler/hw20/input.txt"))) {
            String readLine = reader.readLine();

            while (readLine != null) {
                var split = readLine.split("\\|");
                if (!data.containsKey(split[0])
                        && (Integer.parseInt(split[4]) >= 700)
                        && (Integer.parseInt(split[4]) <= 800)) {
                    data.put(split[0], split);
                }
                readLine = reader.readLine();
            }

            for (var x :
                    data.entrySet()) {
                System.out.println(Arrays.toString(x.getValue()));
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
