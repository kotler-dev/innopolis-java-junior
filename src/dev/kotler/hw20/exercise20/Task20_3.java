package dev.kotler.hw20.exercise20;

/*
o001aa111|Camry|Black|133|790
o002aa111|Camry|Green|0|130
o001aa111|Camry|Black|133|790
o003aa111|Camry|Red|133|700
o004aa111|Camry|Yellow|133|810

* Вывести цвет автомобиля с минимальной стоимостью. // min + map
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Task20_3 {
    public static void main(String[] args) {
        Map<String, String[]> map = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader("src/dev/kotler/hw20/input.txt"))) {
            String readLine = reader.readLine();

            while (readLine != null) {
                var split = readLine.split("\\|");

                if (!map.containsKey(split[0])) {
                    map.put(split[0], split);
                }

                readLine = reader.readLine();
            }

            List<Map.Entry<String, String[]>> black = map.entrySet().stream()
//                    .filter(x -> x.getValue()[4].equals("Green"))
                    .reduce((a, b) -> Integer.parseInt(a.getValue()[4]) < Integer.parseInt(b.getValue()[4]) ? a : b)
//                    .min((p1, p2) -> p1.getAge().compareTo(p2.getAge())).get()
//                    .min((p1, p2) -> p1.getValue()[4].compareTo(p2.getValue()[4]))
//                    .min(new Comparator<Map.Entry<String, String[]>>() {
//                        @Override
//                        public int compare(Map.Entry<String, String[]> o1, Map.Entry<String, String[]> o2) {
//                            return Integer.parseInt(o1.getValue()[4]) < Integer.parseInt(o2.getValue()[4]);
//                        }
//                    })
                    .filter(x -> x.getValue()[1].equals("Camry")).stream().toList();

            black.forEach(x -> System.out.println(Arrays.toString(x.getValue())));

//            data.forEach(x -> System.out.println(Arrays.toString(x)));

            /*for (var dataValue :
                    data.entrySet()) {
                var dataString = dataValue.getValue();
                dataString[2].equals()
            }*/

/*            Arrays.stream(data.values().stream().toList()).

            for (var x :
                    data.entrySet()) {
                System.out.println(Arrays.toString(x.getValue()));
            }*/

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
