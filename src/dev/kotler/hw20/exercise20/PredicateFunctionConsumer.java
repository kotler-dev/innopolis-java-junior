package dev.kotler.hw20.exercise20;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class PredicateFunctionConsumer {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(10, -20, 300, 42, -18);

        Stream<Integer> numbersStream = numbers.stream();
        Predicate<Integer> positiveNumber = number -> number >= 0;
        Function<Integer, String> numberAsStringWithoutZero = number -> number.toString().replaceAll("0", "");
        Consumer<String> printNumber = number -> System.out.println("Number: " + number);

        Stream<Integer> positive = numbersStream.filter(positiveNumber);
        Stream<String> streamWithoutZero = positive.map(numberAsStringWithoutZero);

//        System.out.println(positive.collect(Collectors.toList()));
//        System.out.println(streamWithoutZero.collect(Collectors.toList()));
        streamWithoutZero.forEach(printNumber);
    }
}
