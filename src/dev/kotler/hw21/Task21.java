package dev.kotler.hw21;

import java.util.*;

public class Task21 {
    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {
        Random random = new Random();

        // Сделал зависимость шага от количества элементов и количества потоков
        // Автоматически изменяется длинна хвоста в последнем потоке

        System.out.println("Введите длинну массива: ");
        Scanner scanner = new Scanner(System.in);
//        int arraySize = scanner.nextInt();
        int arraySize = 18;
        System.out.println("Введите количество потоков: ");
//        int threadsCount = scanner.nextInt();
        int threadsCount = 6;


        array = new int[arraySize];
        sums = new int[threadsCount];

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        Arrays.stream(array).forEach(arr -> System.out.print(arr + " "));
        var realSum = Arrays.stream(array).sum();

        System.out.println("\n\nКоличество элементов: " + arraySize);
        System.out.println("Количество потоков: " + threadsCount);
        System.out.println("realSum ==> " + realSum);

        System.out.println("\nРезультат сложения через потоки:");

        // Реализация сложения через потоки
        List<SumElementsInThread> sumElementsInThreads = new ArrayList<>();

        int step = arraySize / threadsCount;
        int firstIndex = 0;
        int lastIndex = 0;
        for (int i = 0; i < threadsCount; i++) {
            // lastIndex - контроль шага чтобы не выйти за границы массива
            lastIndex = firstIndex + step < arraySize ? firstIndex + step + 1 : lastIndex + (arraySize - lastIndex);
            // Создаём каждый элемент массива с экземпляром треда и значениями
            sumElementsInThreads.add(new SumElementsInThread(firstIndex, lastIndex));
            // Счетчик шага
            firstIndex += step + 1;
        }

        // Просмотр созданных индексов для проверки шага
        sumElementsInThreads.forEach(System.out::println);

        for (int i = 0; i < sumElementsInThreads.size(); i++) {
            sumElementsInThreads.get(i).start();
            sums[i] = sumElementsInThreads.get(i).sumOfTread(array);
        }

        int byThreadSum = 0;
        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }
        System.out.println("byThreadSum ==> " + byThreadSum);
    }
}
