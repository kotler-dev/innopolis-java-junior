package dev.kotler.hw21.exercise21;

public class RunnableThread {
    public static void main(String[] args) {
        Runnable taskA = () -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("A");
            }
        };

        Runnable taskB = () -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("B");
            }
        };

        Runnable taskC = () -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("C");
            }
        };



        Thread threadA = new Thread(taskA);
        Thread threadB = new Thread(taskB);
        Thread threadC = new Thread(taskC);

        threadA.start();
        threadB.start();
        threadC.start();

        try {
            threadA.join();
            threadB.join();
            threadC.join();
        } catch (InterruptedException e) {
            throw  new IllegalArgumentException(e);
        }

        for (int i = 0; i < 1000; i++) {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
