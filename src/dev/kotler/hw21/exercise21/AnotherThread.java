package dev.kotler.hw21.exercise21;

public class AnotherThread extends Thread {
    public AnotherThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName());
        }
    }
}
