package dev.kotler.hw21.bank;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorThreadPool {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(4);

        executorService.submit(() -> {
            for (int i = 0; i < 1_0000; i++) {
                System.out.println(Thread.currentThread().getName() + " A");
            }
        });

        executorService.submit(() -> {
            for (int i = 0; i < 1_0000; i++) {
                System.out.println(Thread.currentThread().getName() + " B");
            }
        });

        executorService.submit(() -> {
            for (int i = 0; i < 1_0000; i++) {
                System.out.println(Thread.currentThread().getName() + " C");
            }
        });
    }
}
