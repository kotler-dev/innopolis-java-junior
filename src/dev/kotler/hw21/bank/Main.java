package dev.kotler.hw21.bank;

public class Main {
    public static void main(String[] args) {
        CreditCard card = new CreditCard(1000);
        Human kotler = new Human(card, "Kotler");
        Human juletty = new Human(card, "Juletty");
        juletty.start();
        kotler.start();
    }
}
