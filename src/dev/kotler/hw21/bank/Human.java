package dev.kotler.hw21.bank;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Human extends Thread {
    private final CreditCard card;
    private final String name;

    private static final Lock LOCK = new ReentrantLock(); // Делаем глобальный доступ для всех объектов к LOCK

    public Human(CreditCard card, String name) {
        this.card = card;
        this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 110; i++) {
/*            // Mutex
            synchronized (card) {
                if (card.getAmount() > 0) {
                    System.out.println(name + " будем тратить все твои бабки, вместе! :)");
                    if (card.buy(10)) {
                        System.out.println(name + " что-то купил на " + 10);
                        System.out.println("В кошельке осталось средств: " + card.getAmount());
                    } else {
                        System.out.println(name + " говорит: эээ...");
                    }
                }
            }*/

            // Mutex
            LOCK.lock();
            if (card.getAmount() > 0) {
                System.out.println(name + " будем тратить все твои бабки, вместе! :)");
                if (card.buy(10)) {
                    System.out.println(name + " что-то купил на " + 10);
                    System.out.println("В кошельке осталось средств: " + card.getAmount());
                } else {
                    System.out.println(name + " говорит: эээ...");
                }
            }
            LOCK.unlock();

        }
    }
}
