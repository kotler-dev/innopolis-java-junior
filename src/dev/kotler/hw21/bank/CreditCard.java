package dev.kotler.hw21.bank;

public class CreditCard {
    private int amount; // Общее количество средств

    public CreditCard(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return this.amount;
    }

    public boolean buy(int cost) {
        if (amount >= cost) {
            this.amount -= cost;
            return true;
        } else {
            System.out.println("Недостаточно средств");
            return false;
        }
    }
}
