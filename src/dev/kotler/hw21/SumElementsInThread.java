package dev.kotler.hw21;

public class SumElementsInThread extends Thread {
    private final int startIndex;
    private final int endIndex;

    public SumElementsInThread(int startIndex, int endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    public int sumOfTread(int[] sum) {
        int sums = 0;
        for (int i = startIndex; i < endIndex; i++) {
            System.out.println("startIndex: " + startIndex + " | " + "endIndex: " + endIndex + " | " + "sum[i]" + sum[i]);
            sums += sum[i];
        }
        return sums;
    }

    @Override
    public void run() {
        super.run();
    }

    @Override
    public String toString() {
        return "startIndex=" + startIndex + ", endIndex=" + endIndex;
    }
}
