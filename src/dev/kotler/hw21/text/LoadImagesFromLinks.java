package dev.kotler.hw21.text;

import java.io.*;
import java.net.URL;
import java.util.UUID;

public class LoadImagesFromLinks {
    public static void saveFile(String link) {
        try {
            // Объект для обращения к ссылке
            URL url = new URL(link);
            // Получаем поток байтов по этой сссылке
            InputStream in = new BufferedInputStream(url.openStream());
            // Сделаем буфер, который будет хранить изображение
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            // Массив - буфер
            byte[] buffer = new byte[1024];
            int n;
            while (-1 != (n = in.read(buffer))) {
                out.write(buffer, 0, n);
            }
            out.close();
            in.close();

            // Получаем само изображение
            byte[] image = out.toByteArray();

            // Create file
            String newFileName = UUID.randomUUID().toString();

            // Записываем все байты в новый файл
            FileOutputStream outputStream = new FileOutputStream("src/dev/kotler/hw21/text/" + newFileName + ".jpg");
            outputStream.write(image);
            outputStream.close();

            System.out.println("File saved.");

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static void main(String[] args) {
        // Canada Image
        saveFile("https://sun1-30.userapi.com/impf/wXJTz1kZT1g9egQG69DVbXTwlqpmdWbOzvjdEg/UmULthWTUiQ.jpg?size=489x604&quality=96&sign=8d1fb8226e8b5dac9209628ca5f38a09&type=album");
    }
}
