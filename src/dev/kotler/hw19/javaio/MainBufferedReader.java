package dev.kotler.hw19.javaio;

import java.io.*;

public class MainBufferedReader {
    public static void main(String[] args) {
        try {
            Reader reader = new FileReader("file.txt");
            BufferedReader bufferedReader = new BufferedReader(reader);
            String currentLine = bufferedReader.readLine();

            while (currentLine != null) {
                System.out.println(currentLine);
                currentLine = bufferedReader.readLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
