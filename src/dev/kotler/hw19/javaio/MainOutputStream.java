package dev.kotler.hw19.javaio;

/*
* InputStream
*
* OutputStream -> FileOutputStream
*
* Предназначение: Работа с потоками, бинарными файлами, символами.
*/

import java.io.*;

public class MainOutputStream {
    public static void main(String[] args) {
        try {
            OutputStream outputStream = new FileOutputStream("file-stream.txt");
            byte[] bytes = "Hello from Stream!".getBytes();
            outputStream.write(bytes);
            outputStream.close();
        } catch (IOException e) {
            System.err.println("Не удалось открыть файл.");
        }
    }
}
