package dev.kotler.hw19.javaio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

/*
* Предназначение: Считывание побайтно данных, например, файлв из Интернет.
* */

public class MainInputStream {
    public static void main(String[] args) {
        try {
            InputStream inputStream = new FileInputStream("file-stream.txt");
            int currentByte = inputStream.read();

            while (currentByte != -1) {
                System.out.print((char) currentByte);
                currentByte = inputStream.read();
            }

            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
