package dev.kotler.hw19.repository;

import java.util.List;

public interface UserRepository {
    List<User> findAll();
    void save (User user);
    List<User> findByAge(int age); // find equals age
    List<User> findAllWorkers(); // isWorkers - find all true
}
