package dev.kotler.hw19.repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryFileImpl implements UserRepository {
    private final String fileName;

    public UserRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader buffer = null;

        try {
            reader = new FileReader(fileName);
            buffer = new BufferedReader(reader);

            System.out.println("Данные из файла: ");
            String line = buffer.readLine();

            while (line != null) {
                String[] data = line.split("\\|");
                users.add(new User(data[0], Integer.parseInt(data[1]), Boolean.parseBoolean(data[2])));
                System.out.println(line);

                line = buffer.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException ignore) {
                }
            }
        }

        return users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;

        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorked());
            bufferedWriter.newLine();
            bufferedWriter.flush(); // Сбор буффера для закрытия

            System.out.println("Данные записаны успешно!");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        List<User> user = new ArrayList<>();
        Reader reader = null;
        BufferedReader buffer = null;

        try {
            reader = new FileReader(fileName);
            buffer = new BufferedReader(reader);

            String line = buffer.readLine();
            String[] data;

            while (line != null) {
                data = line.split("\\|");
                user.add(new User(data[0], Integer.parseInt(data[1]), Boolean.parseBoolean(data[2])));
                line = buffer.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException ignore) {
                }
            }
        }

        return user.stream().filter(id -> id.getAge() == age).toList();
    }

    @Override
    public List<User> findAllWorkers() {
        List<User> user = new ArrayList<>();
        Reader reader = null;
        BufferedReader buffer = null;

        try {
            reader = new FileReader(fileName);
            buffer = new BufferedReader(reader);
            String line = buffer.readLine();
            String[] data;

            while (line != null) {
                data = line.split("\\|");
                user.add(new User(data[0], Integer.parseInt(data[1]), Boolean.parseBoolean(data[2])));
                line = buffer.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {}
            }
            if (buffer != null) {
                try {
                    buffer.close();
                } catch (IOException ignore) {}
            }
        }
        return user.stream().filter(User::isWorked).toList();
    }
}
