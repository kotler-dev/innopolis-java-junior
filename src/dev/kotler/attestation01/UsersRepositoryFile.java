package dev.kotler.attestation01;

import java.io.*;
import java.util.*;

public class UsersRepositoryFile implements UsersRepository {

    private final String dataBase;
    private int lastUserID;
    Map<Integer, User> users = new HashMap<>();


    public UsersRepositoryFile(String dataBaseOfUsers) {
        this.dataBase = dataBaseOfUsers;
    }

    @Override
    public Map<Integer, User> findAll() {

        try (BufferedReader reader = new BufferedReader(new FileReader(dataBase))) {
            String line = reader.readLine();
            String[] split = new String[0];

            while (line != null) {
                split = line.split("\\|");
                users.put(Integer.parseInt(split[0]),
                        new User(
                                Integer.parseInt(split[0]),
                                split[1],
                                Integer.parseInt(split[2]),
                                Boolean.parseBoolean(split[3])
                        )
                );
                line = reader.readLine();
            }
            setLastUserID(Integer.parseInt(split[0])); // Запись последнего ID пользователя
            users.values().forEach(System.out::println);
            System.out.println("lastUserID " + lastUserID);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void addNewUserRecord(String userName, int userAge, boolean isWorked) {
        User user = new User(getLastUserID(), userName, userAge, isWorked);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dataBase, true))) {
            writer.write(getLastUserID() + "|" + user.getUserName() + "|" + user.getUserAge() + "|" + user.getIsWorked());
            writer.newLine();
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void findUserByAge(int age) {
        users.values().stream()
                .filter(v -> v.getUserAge() == age)
                .forEach(System.out::println);
    }

    @Override
    public User findById(int id) {
        if (users.get(id) != null) {
            return users.values().stream()
                    .filter(v -> v.getUserID() == id)
                    .map(user -> new User(
                            user.getUserID(),
                            user.getUserName(),
                            user.getUserAge(),
                            user.getIsWorked()
                    )).toList().get(0);
        } else {
            return null;
        }
    }

    @Override
    public void update(User user) {
        users.replace(user.getUserID(), user);
//        users.values().forEach(v -> System.out.println(v));
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(dataBase))) {
            for (User record:
                 users.values()) {
                writer.write( record.getUserID() + "|" + record.getUserName() + "|" + record.getUserAge() + "|" + record.getIsWorked());
                writer.newLine();
            }
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public int getLastUserID() {
        return lastUserID + 1;
    }

    public void setLastUserID(int lastUserID) {
        this.lastUserID = lastUserID;
    }
}
