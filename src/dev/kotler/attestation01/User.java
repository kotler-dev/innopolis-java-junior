package dev.kotler.attestation01;

public class User {
    private int userID;
    private String userName;
    private int userAge;
    private boolean isWorked;

    public User(int userID, String userName, int userAge, boolean isWorked) {
        this.userID = userID;
        this.userName = userName;
        this.userAge = userAge;
        this.isWorked = isWorked;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserAge() {
        return userAge;
    }

    public void setUserAge(int userAge) {
        this.userAge = userAge;
    }

    public boolean getIsWorked() {
        return isWorked;
    }

    public void setIsWorked(boolean worked) {
        isWorked = worked;
    }

    @Override
    public String toString() {
        return getUserID() + " " + getUserName() + " " + getUserAge() + " " + getIsWorked();
    }
}
