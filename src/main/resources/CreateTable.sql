create table vocabulary
(
    id         serial primary key,
    title_list character varying not null
);

create table vocabulary_list
(
    id         serial primary key,
    en         varchar(20) not null,
    ru         varchar(20) not null,
    definition character varying,
    list_id    integer references vocabulary (id)
);