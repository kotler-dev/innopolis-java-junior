select *
from vocabulary;

select *
from vocabulary_list;

select *
from vocabulary_list
where vocabulary_list.list_id = 1;

select title_list
from vocabulary
where vocabulary.id = 1;

-- ---------------------------------------------------------------------------

select (select title_list
        from vocabulary
        where vocabulary.id = vocabulary_list.list_id
       ),
       en,
       ru
from vocabulary_list
where vocabulary_list.list_id = 1;

-- ---------------------------------------------------------------------------

select v.title_list, vl.en, vl.ru
from vocabulary v
         join vocabulary_list vl on v.id = vl.list_id;
-- where vl.list_id = 1;