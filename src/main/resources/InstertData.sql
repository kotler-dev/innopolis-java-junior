insert into vocabulary(id, title_list)
values (1, 'colors'),
       (2, 'food'),
       (3, 'days');

insert into vocabulary_list (en, ru, definition, list_id)
values ('red', 'красный', null, 1),
       ('black', 'чёрный', null, 1),
       ('white', 'белый', null, 1),
       ('apple', 'яблоко', null, 2),
       ('egg', 'яйцо', null, 2),
       ('raspberry', 'малина', null, 2);

-- ---------------------------------------------------------------------------

insert into vocabulary_list (en, ru, definition, list_id)
values ('monday', 'понедельник', null, 3),
       ('tuesday', 'вторник', null, 3),
       ('wednesday', 'среда', null, 3),
       ('thursday', 'четверг', null, 3),
       ('friday', 'пятница', null, 3),
       ('saturday', 'суббота', null, 3),
       ('sunday', 'воскресенье', null, 3);

