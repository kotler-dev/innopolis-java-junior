/*
Создать schema.sql файл, который содержит описание таблиц и данных для этих таблиц

Товар
- id
- описание
- стоимость
- количество

Заказчик
- id
- имя/фамилия

Заказ
- id-товара (внешний ключ)
- id-заказчика (внешний ключ)
- дата заказа
- количество товаров

Написать 3-4 запроса на эти таблицы.
*/

-- langgist - фрагменты для изучения иностранного языка :)

create table student
(
    id        serial primary key,
    firstName varchar(20),
    lastName  varchar(20)
);

create table statistics
(
    id     serial primary key,
    points integer
);

create table vocabulary_list
(
    id         serial primary key,
    en         varchar(20),
    ru         varchar(20),
    definition varchar(300)
);

create table vocabulary
(
    id          serial primary key,
    voc_title   integer,
    voc_list_id integer,
    foreign key (voc_list_id) references vocabulary_list (id)
);