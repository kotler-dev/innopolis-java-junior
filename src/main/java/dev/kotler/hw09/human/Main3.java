package dev.kotler.hw09.human;

public class Main3 {
    public static void main(String[] args) {
        Human human = new Human();
        Developer developer = new Developer();
        Sportsman sportsman = new Sportsman();
        Student student = new Student();

        Human[] humans = new Human[4];
        humans[0] = human;
        humans[1] = developer;
        humans[2] = sportsman;
        humans[3] = student;

//        human.work();
//        developer.work();
//        sportsman.work();
//        student.work();

        for (Human value : humans) {
            value.work();
        }
    }
}
