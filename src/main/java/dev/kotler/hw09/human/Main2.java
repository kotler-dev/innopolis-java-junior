package dev.kotler.hw09.human;

public class Main2 {
    public static void main(String[] args) {
        Human human;
        Sportsman sportsman = new Sportsman();
        // Восходящее преобразование
        human = sportsman;

        human.work();
        sportsman.work();
    }
}
