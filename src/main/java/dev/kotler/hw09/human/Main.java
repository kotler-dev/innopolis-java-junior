package dev.kotler.hw09.human;

public class Main {
    public static void main(String[] args) {
        Human human = new Human();
        Developer developer = new Developer();
        Sportsman sportsman = new Sportsman();
        Student student = new Student();

        human.work();
        developer.work();
        sportsman.work();
        student.work();
    }
}
