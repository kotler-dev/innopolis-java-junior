package dev.kotler.hw09.figure;

// Разбор домашнего задания

/*
Как выяснилось позже, от record нельзя наследоваться.
У record все поля final, а для наследования нужен будет protected.

record Rectangle(int a, int b) {
	public int getArea() {
		return a * b;
	}

	public int getPerimeter() {
		return a * 2 + b * 2;
	}
}
*/

public class Figure {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(10, 15);
        Square square = new Square(5);
        Ellipse ellipse = new Ellipse(3, 4);
        Circle circle = new Circle(42);

        System.out.println(rectangle.getArea());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getArea());
        System.out.println(square.getPerimeter());

        ellipse.paint();
        circle.paint();
    }
}
