package dev.kotler.hw09.figure;

class Ellipse {
    protected double radius1;
    private final double radius2;

    public Ellipse(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public void paint() {
        System.out.println("Ellipse - " + radius1 + " " + radius2);
    }
}
