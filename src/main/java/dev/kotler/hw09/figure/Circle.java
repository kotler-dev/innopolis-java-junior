package dev.kotler.hw09.figure;

class Circle extends Ellipse {
    public Circle(double radius) {
        super(radius, radius);
    }

    public void paint() {
        System.out.println("Circle - " + radius1); // Get protected radius1 from Ellipse
    }
}
