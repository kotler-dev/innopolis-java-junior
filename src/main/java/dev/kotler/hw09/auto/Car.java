package dev.kotler.hw09.auto;

class Car {
    protected int km;

    public Car(int km) {
        this.km = km;
    }

    public void go() {
        System.out.println("Машина проехала: " + km + "км");
    }
}
