package dev.kotler.hw09.auto;

class SportCar extends Car {
    private int speed;

    public SportCar(int km, int speed) {
        super(km);
        this.speed = speed;
    }

    @Override
    public void go() {
        km += speed;
        System.out.println("Суперкар проехал: " + speed);
        System.out.println("Суперкар проехал всего: " + km);
    }
}
