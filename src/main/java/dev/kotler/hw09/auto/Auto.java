package dev.kotler.hw09.auto;

public class Auto {
    public static void main(String[] args) {
        Car car = new Car(10);
        SportCar sportCar = new SportCar(90, 3);

        car.go();
        sportCar.go();
    }
}
