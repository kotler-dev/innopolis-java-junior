package dev.kotler.hw09;

/*
Сделать класс Figure, у данного класса есть два поля - x и y координаты.
Классы Ellipse и Rectangle должны быть потомками класса Figure.
Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
В классе Figure предусмотреть метод getPerimeter(), который возвращает 0.
Во всех остальных классах он должен возвращать корректное значение.
*/

public class Task9 {
    public static void main(String[] args) {
        Figure figure = new Figure();
        Ellipse ellipse = new Ellipse(3, 4);
        Rectangle rectangle = new Rectangle(10, 15);
        Square square = new Square(8);
        Circle circle = new Circle(42);

        System.out.println("Figure getPerimeter: " + figure.getPerimeter());

        ellipse.paint();
        System.out.println("Ellipse getPerimeter: " + ellipse.getPerimeter());

        System.out.println("Rectangle getArea: " + rectangle.getArea());
        System.out.println("Rectangle getPerimeter" + rectangle.getPerimeter());


        System.out.println("Square getArea: " + square.getArea());
        System.out.println("Square getPerimeter: " + square.getPerimeter());

        circle.paint();
        System.out.println("Circle getPerimeter: " + circle.getPerimeter());
    }
}
