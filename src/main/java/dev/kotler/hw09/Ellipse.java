package dev.kotler.hw09;

public class Ellipse extends Figure {
    protected int radius1;
    private final int radius2;

    public Ellipse(int radius1, int radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public void paint() {
        System.out.println("Ellipse paint: " + radius1 + ", " + radius2);
    }

//    @Override
//    public double getPerimeter() {
//        return radius1 * radius2;
//    }
}
