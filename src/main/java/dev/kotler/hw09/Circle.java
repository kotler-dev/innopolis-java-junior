package dev.kotler.hw09;

public class Circle extends Ellipse {
    public Circle(int radius) {
        super(radius, radius);
    }

    public void paint() {
        System.out.println("Circle paint: " + radius1); // Get protected radius1 from Ellipse
    }

    @Override
    public int getPerimeter() {
        return radius1;
    }
}
