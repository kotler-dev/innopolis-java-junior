package dev.kotler.hw11;

/*
Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
Применить паттерн Singleton для Logger.


*/

public class Main {
    public static void main(String[] args) {
        Logger logger = new Logger();
        Logger.getLogger(); // Creating new instance
        logger.logMessage();

        Logger.getLogger(); // Without instance

        logger.logMessage();
        logger.logMessage();
    }
}
