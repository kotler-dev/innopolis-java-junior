package dev.kotler.hw07;

/*
На вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.

Гарантируется:
Все числа в диапазоне от -100 до 100.

Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)
*/

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Task7 {
	public static void main(String[] args) {
		int[] arr = new int[]{42, 42, 13, 0, -1, 42, -93, 13, 0, -1, -1, -93, 3, 3, 7}; // return 7

		int[] result = new int[201];

		for (int j : arr) {
			result[j + 100]++;
		}

		int minIndex = result.length;
		int minValue = Integer.MAX_VALUE;
		for (int i = 0; i < result.length; i++) {
			if (result[i] > 0 && result[i] < minValue) {
				minValue = result[i];
				minIndex = i;
			}
		}
		System.out.println(Arrays.toString(result));
		System.out.println(minIndex - 100);

		findIndex(arr);
	}

	private static void findIndex(int[] arr) {
		Map<Integer, Integer> arrMap = new HashMap<>();
		for (int j : arr) {
			if (arrMap.containsKey(j)) {
				arrMap.put(j, arrMap.get(j) + 1);
			} else {
				arrMap.put(j, 1);
			}
		}

		int minValue = Integer.MAX_VALUE;
		int minKey = Integer.MAX_VALUE;
		for (Integer key : arrMap.keySet()) {
			if (arrMap.get(key) < minValue) {
				minKey = key;
				minValue = arrMap.get(key);
			}
		}
		System.out.println(minKey);
	}
}
