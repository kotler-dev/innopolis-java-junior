package dev.kotler.hw10;

abstract class Figure {
    protected int x;
    protected int y;

    public Figure(int _x, int _y) {
        this.x = _x;
        this.y = _y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    protected void setX(int x) {
        this.x = x;
    }

    protected void setY(int y) {
        this.y = y;
    }

    public void showXY() {
        System.out.printf("X -> %d Y -> %d\n", getX(), getY());
    }

    public double getPerimeter() {
        return 0;
    }
}

