package dev.kotler.hw10;

public interface MoveFigure {
    void move(int x, int y);
}
