package dev.kotler.hw10;

public class Task10 {
    public static void main(String[] args) {
        MoveFigure[] moveFigures = new MoveFigure[2];
        Circle circle = new Circle(10, 18, 42);
        Square square = new Square(13, 30, 111);

        moveFigures[0] = circle;
        moveFigures[1] = square;

        System.out.print("Круг: ");
        circle.showXY();
        System.out.print("Квадрат: ");
        square.showXY();

        for (MoveFigure moveFigure : moveFigures) {
            moveFigure.move(0, 0);
        }

//        moveFigures[0].move(0,0);
//        moveFigures[1].move(0,0);

        System.out.print("Круг: ");
        circle.showXY();
        System.out.print("Квадрат: ");
        square.showXY();
    }
}
