package dev.kotler.hw10.game;

public class Game {
    public static void main(String[] args) {
        Wizard wizard = new Wizard();
        Paladin paladin = new Paladin();
        BlueWater blueWater = new BlueWater();
        RedStone redStone = new RedStone();

        System.out.println("Wizard -> Mana: " + wizard.getMana() + " Health: " + wizard.getHealth() + " Score: " + wizard.getScore());
        System.out.println("Paladin -> Mana: " + wizard.getMana() + " Health: " + wizard.getHealth() + " Score: " + wizard.getScore());

        paladin.setArtefact(redStone);
        wizard.setArtefact(blueWater);
        paladin.attack(wizard);
        wizard.attack(paladin);

        System.out.println("Wizard -> Mana: " + wizard.getMana() + " Health: " + wizard.getHealth() + " Score: " + wizard.getScore());
        System.out.println("Paladin -> " + " Health: " + paladin.getHealth() + " Score: " + paladin.getScore());
    }
}
