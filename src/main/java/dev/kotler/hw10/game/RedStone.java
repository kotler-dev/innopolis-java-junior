package dev.kotler.hw10.game;

public class RedStone implements Artefact {
    @Override
    public int useInAttack() {
        return 10;
    }

    @Override
    public int useInDamage() {
        return 10;
    }
}
