package dev.kotler.hw10.game;

public class Wizard extends Player {
    private static final int DEFAULT_SCORE = 100;
    private static final int DEFAULT_HEALTH = 50;
    private static final int DEFAULT_MANA = 100;
    private static final int ATTACK_MANA = -5;
    private static final int ATTACK_SCORE = 10;
    private static final int DAMAGE_VALUE = 3;
    private static final int HEALTH_AFTER_DAMAGE_VALUE = -2;

    private int mana;

    public Wizard() {
        super(DEFAULT_SCORE, DEFAULT_HEALTH);
        this.mana = DEFAULT_MANA;
    }

    @Override
    public void attack(Player enemy) {
        if (this.mana <= 0) {
            System.err.println("Атака не возможна!");
            return;
        }

        this.mana += ATTACK_MANA;
        this.score += ATTACK_SCORE;
        enemy.damage(DAMAGE_VALUE + artefact.useInAttack());
    }

    @Override
    public void damage(int value) {
        this.health += HEALTH_AFTER_DAMAGE_VALUE * value + artefact.useInDamage();

        if (this.health <= 0) {
            System.err.println("Волшебник проиграл!");
            this.mana = 0;
        }
    }

    public int getMana() {
        return this.mana;
    }
}
