package dev.kotler.hw10.game;

public interface Artefact {
    int useInAttack();
    int useInDamage();
}
