package dev.kotler.hw10.game;

public class BlueWater implements Artefact {
    @Override
    public int useInAttack() {
        return 5;
    }

    @Override
    public int useInDamage() {
        return 5;
    }
}
