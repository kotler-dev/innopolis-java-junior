package dev.kotler.hw10;

public class Square extends Figure implements MoveFigure {
    private int side;

    public Square(int _x, int _y, int side) {
        super(_x, _y);
        setSide(side);
    }

    private void setSide(int side) {
        this.side = side;
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }

    @Override
    public double getPerimeter() {
        return side * 4;
    }
}
