package dev.kotler.hw10;

interface StrInterface<T> {
    void print(T t);
}

public class PrintStringInterface {
    public static void main(String[] args) {
        StrInterface<String> str = System.out::println;
        str.print("Трам-пам-пам!");
    }
}
