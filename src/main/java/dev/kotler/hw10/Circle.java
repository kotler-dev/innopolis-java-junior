package dev.kotler.hw10;

public class Circle extends Figure implements MoveFigure {
    private int radius;

    public Circle(int _x, int _y, int _radius) {
        super(_x, _y);
        setRadius(_radius);
    }

    @Override
    public double getPerimeter() {
        return this.radius * 2 * 3.14;
    }

    private void setRadius(int radius) {
        this.radius = radius;
    }

    private void showRadius(int radius) {
        System.out.println(this.radius);
    }

    @Override
    public void move(int x, int y) {
        setX(x);
        setY(y);
    }
}
