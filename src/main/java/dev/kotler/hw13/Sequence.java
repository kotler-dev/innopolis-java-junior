package dev.kotler.hw13;

import java.util.ArrayList;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition, boolean flag) {
        ArrayList<Integer> arr = new ArrayList<>();

        for (int j : array) {
            if (condition.isOk(j, flag)) {
                arr.add(j);
            }
        }

        int[] arr2 = new int[arr.size()];

        for (int i = 0; i < arr.size(); i++) {
            arr2[i] = arr.get(i);
        }

        return arr2;
    }
}
