package dev.kotler.hw13;

/*
Предусмотреть функциональный интерфейс

interface ByCondition {
	boolean isOk(int number);
}

Реализовать в классе Sequence метод:
public static int[] filter(int[] array, ByCondition condition) {
	...
}

Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.

В main в качестве condition подставить:
- проверку на четность элемента
- проверку, является ли сумма цифр элемента четным числом.
*/

import java.util.Arrays;

import static dev.kotler.hw13.Sequence.filter;

public class Task13 {
    public static void main(String[] args) {

        ByCondition condition = (number, flag) -> {
            if ((number % 2 == 0) == flag) {
                int num = number;
                int sum = 0;

                while (num > 0) {
                    sum = sum + num % 10;
                    num = num / 10;
                }
                return (sum % 2 == 0) == flag;
            }
            return false;
        };

        // Flag
        // true - поиск чётных чистел
        // false - поиск нечётных чисел

        int[] result1 = filter(new int[]{111, 222, 333, 444, 77, 88}, condition, true);
        System.out.println(Arrays.toString(result1));


        int[] result2 = filter(new int[]{111, 222, 333, 444, 77, 88}, condition, false);
        System.out.println(Arrays.toString(result2));
    }
}
