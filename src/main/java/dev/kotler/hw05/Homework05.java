package dev.kotler.hw05;

// https://gitlab.com/maxima_it_school/pcs_java_21_02/-/tree/master/Projects/03.%20Algorithms
// Задание 05: Найти минимальную цифру во всех последовательностях.

/*
Реализовать программу на Java, которая для последовательности чисел,
оканчивающихся на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

Например:
345
298
456
-1

Ответ: 2
*/

import java.util.Arrays;
import java.util.Scanner;

public class Homework05 {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		System.out.println("Задание 05: Найти минимальную цифру во всех последовательностях.");
		System.out.println("Введите последовательность цифр: ");
		String[] input = scanner.next().split("");

		int minValueSequence = Integer.MAX_VALUE; // Для поиска мин числа во всех последовательностях

		while (input[0].charAt(0) != '-') {
			for (String x : input) {
				if (Integer.parseInt(x) < minValueSequence) {
					minValueSequence = Integer.parseInt(x);
					System.out.println("Новое минимальное число: " + minValueSequence);
				}
			}
			System.out.println("Введите последовательность цифр: ");
			input = scanner.next().split("");
		}
		System.out.println("Минимальное число во всех последовательностях: " + minValueSequence);
		System.out.println("Программа завершена.");
	}
}
