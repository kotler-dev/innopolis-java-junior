package dev.kotler.hw24;

import java.util.HashMap;
import java.util.Map;

public class ColumnCord {
    public static void main(String[] args) {
        /*
        column_cord = {"R1": ((87, 10), (157, 539)), "R2": ((161, 10), (232, 539)),
               "R3": ((235, 10), (313, 539)), "R4": ((315, 10), (382, 539)),
        */

        class Point {
            final int x;
            final int y;

            public Point(int x, int y) {
                this.x = x;
                this.y = y;
            }
            @Override
            public String toString() {
                return "(" + this.x + "," + this.y + ")";
            }
        }
        class Dimension {
            final Point position;
            final Point size;

            public Dimension(Point position, Point size) {
                this.position = position;
                this.size = size;
            }
        }

        Map<String, Dimension> columnCord = new HashMap<>();
        columnCord.put("R1", new Dimension(new Point(1, 2), new Point(2, 3)));
        columnCord.put("R2", new Dimension(new Point(331, 23), new Point(223, 31)));

        columnCord.entrySet().stream()
                .map(v -> v.getKey() + " " + v.getValue().position.toString() + " " + v.getValue().size.toString())
                .forEach(v -> System.out.println(v));
    }
}
