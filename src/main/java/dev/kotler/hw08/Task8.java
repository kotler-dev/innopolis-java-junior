package dev.kotler.hw08;

/*
На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
Считать эти данные в массив объектов.
Вывести в отсортированном по возрастанию веса порядке.
*/

class Human {
	private String name;
	private double weight;

	public void setName(String name) {
		this.name = name;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public double getWeight() {
		return weight;
	}
}

public class Task8 {
	public static void main(String[] args) {
		Human[] humans = new Human[4]; // Массив объектов класса Human
		Human[] human = new Human[1]; // Массив для перестановки объектов

//		humans[0] = new Humans("Motya", 74); // Можно создать через конструктор в классе
		humans[0] = new Human();
		humans[0].setName("Vasya");
		humans[0].setWeight(80);

		humans[1] = new Human();
		humans[1].setName("Anton");
		humans[1].setWeight(180);

		humans[2] = new Human();
		humans[2].setName("Kostya");
		humans[2].setWeight(30);

		humans[3] = new Human();
		humans[3].setName("Kotler");
		humans[3].setWeight(10);

		System.out.println("\nСозданные объекты: \n");

		for (Human item : humans) {
			System.out.println("Name: " + item.getName() + ", Weight: " + item.getWeight());
		}

		// Сортировка
		// Большее число перемещается в конец массива
		// Поэтому мы можем каждый раз отрезать хвост с помощью (humans.length - i)
		// Так как мы знаем, что в конце лежит самое большое значение
		for (int i = 0; i < humans.length; i++) {
			for (int j = 1; j < (humans.length - i); j++) {
				if (humans[j - 1].getWeight() > humans[j].getWeight()) {
					human[0] = humans[j];
					humans[j] = humans[j - 1];
					humans[j - 1] = human[0];
				}
			}
		}

		System.out.println("\nОтсортированные объекты по ключу [Weight]: \n");

		for (Human value : humans) {
			System.out.println("Name: " + value.getName() + ", Weight: " + value.getWeight());
		}
	}
}
