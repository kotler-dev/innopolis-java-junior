package dev.kotler.hw06;

/*
Реализовать функцию, принимающую на вход массив и целое число.
Данная функция должна вернуть индекс этого числа в массиве.
Если число в массиве отсутствует - вернуть -1.

Реализовать процедуру, которая переместит все значимые элементы влево, заполнив нулевые, например:

было:
34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20

стало
34, 14, 15, 18, 1, 20, 0, 0, 0, 0, 0
*/

import java.util.Arrays;

public class Task6 {
	public static void main(String[] args) {
		//
		int[] arr = new int[]{34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

		// Функци принимает маммив и искомое число
		// Возвращает индекс найденного числа или -1 если не найден
		int result = findIndexOfElement(arr, 18); // return index 6
		System.out.println(result);

		// Перемещение значимых элементов в левую сторону
		sortItemsToLeft(arr);

		// Дополнительное решение с использованием .filter и создания нового массива
		filterItems(arr);
	}

	public static int findIndexOfElement(int[] arrayLocal, int value) {
		int index = -1;

		for (int i = 0; i < arrayLocal.length; i++) {
			if (value == arrayLocal[i]) {
				index = i;
			}
		}
		return index;
	}

	public static void sortItemsToLeft(int[] arrayReplace) {
		System.out.println(Arrays.toString(arrayReplace));

		for (int i = 0; i < arrayReplace.length; i++) {
			if (arrayReplace[i] == 0) {
				for (int j = i; j < arrayReplace.length; j++) {
					if (arrayReplace[j] != 0) {
						arrayReplace[i] = arrayReplace[j];
						arrayReplace[j] = 0;
						break;
					}
				}
			}
		}
		System.out.println(Arrays.toString(arrayReplace));
	}

	// Дополнительное решение с использованием .filter и создания нового массива
	private static void filterItems(int[] arr) {
		int ln = arr.length;
		var arr2 = Arrays.stream(arr).filter(x -> x != 0).toArray();
		System.out.println(Arrays.toString(arr2));
		int[] arr3 = new int[ln];

		// Idea предложили цикл заменить на arraycopy
		System.arraycopy(arr2, 0, arr3, 0, arr2.length);

		System.out.println(Arrays.toString(arr3));
	}
}

