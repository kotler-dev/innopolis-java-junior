package dev.kotler.hw03;

// https://gitlab.com/maxima_it_school/pcs_java_21_02/-/tree/master/Projects/03.%20Algorithms
// Задание 1: Вывести сумму всех чисел последовательности.

import java.util.Scanner;

public class Task1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Задание: Вывести сумму всех чисел последовательности");
		System.out.print("Введите число от 0 до Integer.MAX_VALUE (exit -1): ");
		int value = scanner.nextInt();
		int sumAllElements = 0;

		while (value != -1) {
			sumAllElements += value;
			System.out.print("Введите число (exit -1): ");
			value = scanner.nextInt();
		}

		System.out.println("Сумма всех элементов: " + sumAllElements);
		System.out.println("Программа завершена.");
	}
}
