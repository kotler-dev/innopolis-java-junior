package dev.kotler.hw03;

// https://gitlab.com/maxima_it_school/pcs_java_21_02/-/tree/master/Projects/03.%20Algorithms
// Задание 4: Вывести количество локальных минимумов: ai - локальный минимум, если ai-1 > ai < ai+1

/*
Example:
46
23 -> локальный минимум
55
120
33 -> локальный минимум
400
21 -> локальный минимум
500

Ответ: 3
*/

import java.util.Arrays;

public class Task4 {
	public static void main(String[] args) {
		Integer[] ai = {46, 23, 55, 120, 33, 400, 21, 500};
		System.out.println("Задание 4: Вывести количество локальных минимумов.");
		System.out.println("Условие: ai - локальный минимум, если ai-1 > ai < ai+1");
		System.out.println("Данные: " + Arrays.toString(ai));

		int inc = 0; // Счётчик найденных минимумов
		for (int i = 1; i < ai.length; i++) {
			if (ai[i - 1] > ai[i] && ai[i] < ai[i + 1]) {
				System.out.println("Локальный минимум: " + ai[i]);
				inc++;
			}
		}
		System.out.println("Количество найденных минимумов: " + inc);
		System.out.println("Программа завершена.");
	}
}
