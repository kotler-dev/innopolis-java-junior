package dev.kotler.hw03;

// https://gitlab.com/maxima_it_school/pcs_java_21_02/-/tree/master/Projects/03.%20Algorithms
// Задание 3: Найти минимум среди всех чисел последовательности.


import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {
		System.out.println("Задание 3: Найти минимум среди всех чисел последовательности.");
		Scanner scanner = new Scanner(System.in);
		int value = scanner.nextInt();

		int digitsSum;
		int minDigitsSum = -1;

		while (value != -1) {
			digitsSum = 0;

			if (value % 2 == 0) {
				while (value != 0) {
					// Получение последнего значения последовательности числа
					int lastDigit = value % 10;
					digitsSum += lastDigit;
					value = value / 10;

				}
				System.out.println("Сумма последовательности -> " + digitsSum);

				// Записать первую минимальную сумму последовательности для сравнения
				if (minDigitsSum == -1) {
					minDigitsSum = digitsSum;
				}
				// Сохранить вновь найденную минимальную сумму последовательности
				if (digitsSum < minDigitsSum) {
					minDigitsSum = digitsSum;
				}
			} else {
				System.out.println("Вы ввели " + value + " - нечётное число.");
			}
			System.out.print("Введите число (exit -1): ");
			value = scanner.nextInt();
		}
		System.out.println("Минимальное число из всех последовательностей: " + minDigitsSum);
		System.out.println("Программа завершена.");
	}
}

