package dev.kotler.hw03;

// https://gitlab.com/maxima_it_school/pcs_java_21_02/-/tree/master/Projects/03.%20Algorithms
// Задача 2: Посчитать количество всех четных чисел и сумму всех нечетных чисел.

import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		System.out.println("Задача 2: Посчитать количество всех чётных чисел и сумму всех нечетных чисел.");
		Scanner scanner = new Scanner(System.in);
		System.out.print("Введите число (exit -1): ");
		int value = scanner.nextInt();
		int sumEvens = 0;
		int sumOdds = 0;

		while (value != -1) {
			if (value % 2 == 0) {
				sumEvens++;
			} else {
				sumOdds++;
			}
			System.out.print("Введите число (exit -1): ");
			value = scanner.nextInt();
		}

		System.out.println("Сумма чётных чисел: " + sumEvens);
		System.out.println("Сумма не чётных чисел: " + sumOdds);
		System.out.println("Программа завершена.");
	}
}
