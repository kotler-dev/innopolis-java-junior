package dev.kotler.hw16;

/*
Реализовать removeAt(int index) для ArrayList
Реализовать метод T get(int index) для LinkedList
*/

import java.util.Arrays;

public class Task16 {
    public static void main(String[] args) {
        System.out.println("Реализация ArrayList");

        ArrayList<Integer> array = new ArrayList<>();

        array.add(5);
        array.add(18);
        array.add(42);
        array.add(3);
        array.add(808);
        array.add(421);
        array.add(777);
        array.print(); // Вывод массива со всеми элементами

        array.removeAt(2); // Удаление элемент с индексом 2
        array.print(); // Вывод обновленного массива после удаления элемента

        array.clear(); // Очистка массива
        array.print();

        array.add(808);
        array.add(421);
        array.add(777);
        array.print();

        System.out.println("\nРеализация LinkedList");

        LinkedList<Integer> linkedList = new LinkedList<>();
        linkedList.add(5);
        linkedList.add(18);
        linkedList.add(42);
        linkedList.add(3);
        linkedList.add(808);
        linkedList.add(421);
        linkedList.add(777);
        linkedList.print();

        System.out.println("LinkedList.get(2) -> " + linkedList.get(2)); // 42
    }
}
